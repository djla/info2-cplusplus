#include "rectangle.h"
#include <iostream>

using namespace std;

Rectangle::Rectangle()
{
    cout << "Constructor predeterminado de la clase Rectangle" << endl;
    m_width = 0;
    m_height = 0;
}

Rectangle::Rectangle(int x, int y, unsigned int width, unsigned int height)
    : Shape (x, y)
{
    cout << "Constructor parametrizable de la clase Rectangle" << endl;
    m_width = width;
    m_height = height;
}

unsigned int Rectangle::getWidth() const
{
    return m_width;
}

unsigned int Rectangle::getHeight() const
{
    return m_height;
}

void Rectangle::setWidth(unsigned int width)
{
    m_width = width;
}

void Rectangle::setHeight(unsigned int height)
{
    m_height = height;
}

void Rectangle::printPosition() const
{
    cout << "Posicion: " << getX() << ", " << getY() << endl;   // Se implementa este metodo para mostrar como acceder a los atributos privados de la clase base
}

void Rectangle::draw()
{
    cout << "Soy un rectangulo" << endl;
    Shape::draw();
    cout << "Dimensiones: " << m_width << " x " << m_height << endl;
}
