#include "shape.h"
#include <iostream>

using namespace std;

Shape::Shape()
{
    cout << "Constructor predeterminado. Direccion del objeto Shape: " << this << endl;
    x = 0;
    y = 0;
}

Shape::Shape(const Shape &source)
{
    cout << "Constructor de copia. Direccion del objeto Shape: " << this << endl;
    x = source.x;
    y = source.y;
}

Shape::Shape(int xc, int yc)
{
    cout << "Constructor parametrizable. Direccion del objeto Shape: " << this << endl;
    x = xc;
    y = yc;
}

Shape::~Shape()
{
    cout << "Destructor. Direccion del objeto Shape: " << this << endl;
}

Shape & Shape::moveBy(int dx, int dy)
{
    this->x += dx;  // Uso explicito del apuntador this
    this->y += dy;  // Uso explicito del apuntador this
    return *this;   // Retorna el contenido del apuntador this.
}

int Shape::getX(void) const
{
    return x;
}

int Shape::getY(void) const
{
    return y;
}

void Shape::draw()
{
    cout << "Posicion: " << x << ", " << y << endl;
}

// Implementacion de funciones externas a la clase
void AddOneToX_NoFriend(Shape &s)
{
    s.moveBy(1, 0);
}

void AddOneToX_Friend(Shape &s)
{
    s.x++;
}
