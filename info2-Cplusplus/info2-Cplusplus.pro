TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    shape.cpp \
    stack.cpp \
    rectangle.cpp \
    circle.cpp \
    list.cpp

HEADERS += \
    shape.h \
    stack.h \
    rectangle.h \
    circle.h \
    list.h
