/*
 * Implementación del manejo de un stack (o pila) - Implementacion en C++
 * Se modifica la implementacion usando una lista simplemente enlazada
 * Recordar de la clase de repaso (donde se implementó el stack en lenguaje C)
 * que el el stack trabaja en modo LIFO (last in, first out)
 *
 * Observación: La biblioteca estandar de C++ cuenta con una clase para el manejo de un stack,
 * y sus métodos no son exactamente los mismos que los planteados aqui.
 */

#ifndef STACK_H
#define STACK_H

template <class T>
class Stack
{
    struct Node         // Estructura de los nodos de la lista
    {
        T data;         // En este caso, el dato es un int, pero podria ser cualquier tipo de datos, incluso un tipo de dato definido por el usuario
        Node *next;     // Apunta al proximo nodo de la lista
        Node(const T &data, Node *next) {this->data = data; this->next = next;}
    };

public:
    Stack();                        // Constructor por defecto
    ~Stack();                       // Destructor
    void push(const T &data);       // Inserta un dato en el stack
    T pop();                        // Remueve un dato del stack
    bool isEmpty() const;           // Retorna true si el stack está vacio y false en caso contrario
    const T& operator[](unsigned int index);  // Sobrecarga del operador []. Permite acceder a un elemento del stack

private:
    Node *head;                         // Puntero "cabeza" de la lista, siempre apuntara al primer nodo de la lista.
    Stack(const Stack&);                // Constructor de copia privado, para el que el objeto no sea copiable
    Stack & operator=(const Stack &);   // Sobrecarga del operador de asignacion privado, para que el objeto no sea copiarse
};

#include <iostream>

template <class T>
Stack<T>::Stack()
{
    head = nullptr;     // Indica que la lista esta vacia
    std::cout << "Inicializa el stack" << std::endl;
}

template <class T>
Stack<T>::~Stack()
{
    std::cout << "Libera el espacio asignado para el stack" << std::endl;

    Node *oldHead;
    while(head != nullptr)
    {
        oldHead = head;
        head = oldHead->next;
        delete oldHead;
    }
}

template <class T>
void Stack<T>::push(const T &data)
{
    // Obtiene memoria en forma dinamica para un nuevo nodo (se insertara al comienzo de la lista)
    Node *firstNode = new Node(data, head); // Inicializa el nodo con el constructor parametrizable

    // Modifica head, ahora apunta al nuevo nodo de la lista
    head = firstNode;

    std::cout << "Se agrega un dato en el stack: " << data << std::endl;
}

template <class T>
T Stack<T>::pop()
{
    T data;

    if (!isEmpty())
    {
        data = head->data;          // Obtiene el dato (que debera luego retornar)

        Node *oldHead = head;       // Usa un puntero temporal para guardar la direccion del primer nodo de la lista
        head = head->next;          // Actualiza el inicio de la lista (ahora el segundo nodo de la lista pasara a ser el primero)
        delete oldHead;             // Libera la memoria que ocupaba el primer nodo de la lista

        std::cout << "Se remueve un dato en el stack: " << data << std::endl;
    }
    else
    {
        std::cout << "Stack vacio" << std::endl;
    }

    return data;
}

template <class T>
bool Stack<T>::isEmpty() const
{
    return (head == nullptr) ? true : false;
}

template <class T>
const T& Stack<T>::operator[](unsigned int index)
{
    Node *tmp = head;

    while(tmp != nullptr)
    {
        if (index == 0)
        {
            return tmp->data;
        }
        else
        {
            tmp = tmp->next;
            index--;
        }
    }

    // Si llego aqui es porque index esta fuera de rango
    std::cout << "Index fuera de rango" << std::endl;
    exit(1);
}

#endif // STACK_H
