#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.h"

class Rectangle : public Shape          // Herencia simple (Clase base Shape, clase derivada Rectangle)
{

public:
    Rectangle();                                                        // Constructor por defecto
    Rectangle(int x, int y, unsigned int width, unsigned int height);   // Constructor parametrizado
    unsigned int getWidth() const;
    unsigned int getHeight() const;
    void setWidth(unsigned int width);
    void setHeight(unsigned int height);
    void printPosition() const;
    void draw();

private:
    unsigned int m_width;      // Se utiliza para guardar el ancho de la figura rectangulo
    unsigned int m_height;     // Se utiliza para guardar el alto de la figura rectangulo

};

#endif // RECTANGLE_H
