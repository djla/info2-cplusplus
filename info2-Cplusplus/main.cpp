// Listas simplemente enlazadas
// Implementacion de Stack con template.

#include <iostream>
#include "stack.h"

using namespace std;

int main ()
{
    cout << "***********************************" << endl;
    cout << "Trabajo con el stack s1 de <int>" << endl;
    cout << "***********************************" << endl;
    Stack<int> s1;		// Crea un stack de int

    s1.push(10);		// Almacena en forma temporal el valor 10
    s1.push(20);		// Almacena en forma temporal el valor 20
    s1.push(30);		// Almacena en forma temporal el valor 30

    cout << "dato en posicion [0] = " << s1[0] << endl;
    cout << "dato en posicion [1] = " << s1[1] << endl;
    cout << "dato en posicion [2] = " << s1[2] << endl;

    while(!s1.isEmpty())
    {
        s1.pop();		// Remueve los datos del stack
    }

    cout << "\n***********************************" << endl;
    cout << "Trabajo con el stack s2 de <string>" << endl;
    cout << "***********************************" << endl;
    Stack<string> s2;	// Crea un stack de string

    s2.push("https://www.google.com.ar/");          // Almacena en forma temporal la primera página visitada
    s2.push("https://www.google.com.ar/maps/");		// Almacena en forma temporal la segunda página visitada
    s2.push("https://bitbucket.org/product/");		// Almacena en forma temporal la tercera página visitada

    cout << "dato en posicion [0] = " << s2[0] << endl;
    cout << "dato en posicion [1] = " << s2[1] << endl;
    cout << "dato en posicion [2] = " << s2[2] << endl;

    while(!s2.isEmpty())
    {
        s2.pop();		// Remueve los datos del stack
    }

    return 0;
}
