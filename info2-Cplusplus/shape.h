#ifndef SHAPE_H
#define SHAPE_H


class Shape
{
    // La siguiente funcion es "amiga" de la clase. Lo que hará es incrementar en uno la coordenda x
    // Importante. No es una funcion miembro de la clase, por el contrario, es externa a la misma
    friend void AddOneToX_Friend(Shape &s);

public:
    Shape();                        // Constructor predeterminado
    Shape(const Shape &source);     // Constructor de copia
    Shape(int xc, int yc);          // Constructor con argumentos (sobrecarga)
    virtual ~Shape();                       // Destructor

    Shape & moveBy(int dx, int dy);    // Desplaza la figura (o shape) en los valores pasados como parametro
    int getX(void) const;           // Retorna la "coordenada X" donde esta ubicada la figura
    int getY(void) const;           // Retorna la "coordenada Y" donde esta ubicada la figura
    virtual void draw();                    // Permite dibujar la figura

private:
    int x;  // Se utiliza para guardar la coordenada X donde esta ubicada la figura
    int y;  // Se utiliza para guardar la coordenada Y donde esta ubicada la figura
};

// La siguiente funcion no es "amiga" de la clase.
// Al igual que AddOneToX_Friend() es externa y realizara la misma operacion de incrementar en uno la coordenada x
void AddOneToX_NoFriend(Shape &s);


#endif // SHAPE_H
