#include "circle.h"
#include <iostream>

using namespace std;

Circle::Circle()
{
    cout << "Constructor predeterminado de la clase Circle" << endl;
    m_radius = 0;
}

Circle::Circle(int x, int y, unsigned int radius)
    : Shape (x, y)
{
    cout << "Constructor parametrizable de la clase Circle" << endl;
    m_radius = radius;
}

unsigned int Circle::getRadius() const
{
    return m_radius;
}


void Circle::setRadius(unsigned int radius)
{
    m_radius = radius;
}

void Circle::draw()
{
    cout << "Soy un circulo" << endl;
    Shape::draw();
    cout << "Radio: " << m_radius << endl;
}

