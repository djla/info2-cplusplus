#ifndef LIST_H
#define LIST_H

#include <iostream>

class List
{
    friend std::ostream& operator<< (std::ostream& out, const List &l); // Imprime todos los elementos de la lista

    struct Node             // Estructura de los nodos de la lista
    {
        int data;           // En este caso, el dato es un int, pero podria ser cualquier tipo de datos, incluso un tipo de dato definido por el usuario
        Node *next;         // Apunta al proximo nodo de la lista
        Node(const int& data, Node *next) {this->data = data; this->next = next;}
    };

public:
    // Constructores y destructor
    List();
    List(const List& source);

    // Métodos relacionados con la capacidad de la lista
    bool isEmpty() const;                   // Retorna si la lista esta vacia o no
    unsigned int size() const;              // Retorna la cantidad de nodos que contiene la lista

    // Métodos para acceder a los elementos
    const int& front() const;              // Retorna una referencia al primer elemento de la lista (si la lista esta vacia, finaliza el programa */
    const int& back() const;               // Retorna una referencia al último elemento de la lista (si la lista esta vacia, finaliza el programa */

    // Métodos vinculados a la inserción/extracción de elementos en la lista
    void push_front(const int& val);        // Inserta un elemento al inicio
    void pop_front();                       // Remueve el primer elemento
    void push_back(const int& val);         // Inserta un elemento al final de la lista
    void pop_back();                        // Remueve el último elemento de la lista

    // Sobrecarga de operadores
    List& operator=(const List& source);       // Operador de asignacion
    const int& operator[](unsigned int index);  // Operador []. Permite acceder a un elemento de la lista para su lectura

    private:
    Node *head;

};

#endif // LIST_H
