#ifndef CIRCLE_H
#define CIRCLE_H

#include "shape.h"

class Circle : public Shape
{
public:
    Circle();                                       // Constructor por defecto
    Circle(int x, int y, unsigned int radius);      // Constructor parametrizado
    unsigned int getRadius() const;
    void setRadius(unsigned int radius);
    void draw();

private:
    unsigned int m_radius;

};

#endif // CIRCLE_H
